import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AuthentificationService } from 'src/app/services/authentification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email: string;
  password: string;

  constructor(private users: UsersService,
    public authService: AuthentificationService,
    private router: Router) { 
  }

  ngOnInit() {
  }

  logIn(email, password) {
    this.authService.SignIn(email, password)
      .then((res) => {
        if(email === 'admin@gmail.com'){
          this.router.navigate(['home-admin']);
        }else{
          this.router.navigate(['home']);
        }
      }).catch((error) => {
        alert(error.message)
      })
  }

}
