export class User {
    uid: string;
    email: string;
    username: string;
    picture: string;
    phone: string;
    role: boolean;
    password: string;
    emailVerified : boolean; 
}
