import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RequetesAdminPage } from './requetes-admin.page';

const routes: Routes = [
  {
    path: '',
    component: RequetesAdminPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RequetesAdminPageRoutingModule {}
