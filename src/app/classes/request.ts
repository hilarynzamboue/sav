
export class Request {
    id: string;
    titre: string;
    date: string;
    produit: string;
    numserie: string;
    boutique: string;
    description: string;
    etat: string;
    user: string;

}
