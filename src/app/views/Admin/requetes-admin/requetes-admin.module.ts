import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RequetesAdminPageRoutingModule } from './requetes-admin-routing.module';

import { RequetesAdminPage } from './requetes-admin.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RequetesAdminPageRoutingModule
  ],
  declarations: [RequetesAdminPage]
})
export class RequetesAdminPageModule {}
