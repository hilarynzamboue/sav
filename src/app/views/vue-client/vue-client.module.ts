import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VueClientPageRoutingModule } from './vue-client-routing.module';

import { VueClientPage } from './vue-client.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VueClientPageRoutingModule
  ],
  declarations: [VueClientPage]
})
export class VueClientPageModule {}
