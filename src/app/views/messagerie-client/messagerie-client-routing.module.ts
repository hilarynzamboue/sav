import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MessagerieClientPage } from './messagerie-client.page';

const routes: Routes = [
  {
    path: '',
    component: MessagerieClientPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MessagerieClientPageRoutingModule {}
