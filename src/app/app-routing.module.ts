import { NgModule } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'messagerie',
    loadChildren: () => import('./views/messagerie/messagerie.module').then( m => m.MessageriePageModule)
  },
  {
    path: 'important',
    loadChildren: () => import('./views/important/important.module').then( m => m.ImportantPageModule)
  },
  {
    path: 'media',
    loadChildren: () => import('./views/media/media.module').then( m => m.MediaPageModule)
  },
  {
    path: 'compte',
    loadChildren: () => import('./views/compte/compte.module').then( m => m.ComptePageModule)
  },
  {
    path: 'parametres',
    loadChildren: () => import('./views/parametres/parametres.module').then( m => m.ParametresPageModule)
  },
  {
    path: 'faq',
    loadChildren: () => import('./views/faq/faq.module').then( m => m.FaqPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./views/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'signup',
    loadChildren: () => import('./views/signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'infos',
    loadChildren: () => import('./views/infos/infos.module').then( m => m.InfosPageModule)
  },
  {
    path: 'fogotedpwd',
    loadChildren: () => import('./views/fogotedpwd/fogotedpwd.module').then( m => m.FogotedpwdPageModule)
  },
  {
    path: 'accueil',
    loadChildren: () => import('./views/accueil/accueil.module').then( m => m.AccueilPageModule)
  },
  {
    path: 'requetes-admin',
    loadChildren: () => import('./views/Admin/requetes-admin/requetes-admin.module').then( m => m.RequetesAdminPageModule)
  },
  {
    path: 'liste-clients',
    loadChildren: () => import('./views/Admin/liste-clients/liste-clients.module').then( m => m.ListeClientsPageModule)
  },
  {
    path: 'details-requete',
    loadChildren: () => import('./views/Admin/details-requete/details-requete.module').then( m => m.DetailsRequetePageModule)
  },
  {
    path: 'verify-email',
    loadChildren: () => import('./views/verify-email/verify-email.module').then( m => m.VerifyEmailPageModule)
  },
  {
    path: 'messagerie-client',
    loadChildren: () => import('./views/messagerie-client/messagerie-client.module').then( m => m.MessagerieClientPageModule)
  },
  {
    path: 'home-admin',
    loadChildren: () => import('./home-admin/home-admin.module').then( m => m.HomeAdminPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
