import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { Router } from '@angular/router';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {

  constructor(
    public actionSheetController: ActionSheetController,
    private router:Router
  ) { }

  async ShowSheet(pers) {
    const actionSheet = await this.actionSheetController.create({
      header: pers.nom,
      buttons: [
        {
          text: 'Message',
          icon: 'mail-sharp',
          handler: () => {
            console.log('Call clicked');
            this.router.navigate(['vue-gerant']);
          }
        }, {
        text: 'Infos',
        role: '',
        icon: 'information-circle',
        handler: () => {
          console.log('Delete clicked');
        }
      }, {
        text: 'Partager',
        icon: 'share',
        handler: () => {
          console.log('Share clicked');
        }
      }, {
        text: 'Supprimer',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          console.log('Delete clicked');
        }
      }, {
        text: 'Retour',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
  
    await actionSheet.present();
  }

  
  personnes = [
    {
      image: "assets/Moi.jpg",
      nom: "Dame Nature"
    },
    {
      image: "assets/Moi.jpg",
      nom: "Hilary Nzamboue"
    },
    {
      image: "assets/Moi.jpg",
      nom: "Natacha Nzamboue"
    },
    {
      image: "assets/Moi.jpg",
      nom: "Vanessa Zamboue"
    },
    {
      image: "assets/Moi.jpg",
      nom: "Marvin Bidzana"
    },
  ];


  ngOnInit() {
  }

}
