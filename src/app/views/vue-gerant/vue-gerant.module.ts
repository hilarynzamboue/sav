import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VueGerantPageRoutingModule } from './vue-gerant-routing.module';

import { VueGerantPage } from './vue-gerant.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VueGerantPageRoutingModule
  ],
  declarations: [VueGerantPage]
})
export class VueGerantPageModule {}
