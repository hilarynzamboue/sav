import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  chemin='assets/users.json';

  constructor(private http: HttpClient) { }

  getUsers(){
    return this.http.get(this.chemin);
  }

}
