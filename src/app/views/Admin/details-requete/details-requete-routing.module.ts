import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailsRequetePage } from './details-requete.page';

const routes: Routes = [
  {
    path: '',
    component: DetailsRequetePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailsRequetePageRoutingModule {}
