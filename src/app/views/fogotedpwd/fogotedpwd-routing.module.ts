import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FogotedpwdPage } from './fogotedpwd.page';

const routes: Routes = [
  {
    path: '',
    component: FogotedpwdPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FogotedpwdPageRoutingModule {}
