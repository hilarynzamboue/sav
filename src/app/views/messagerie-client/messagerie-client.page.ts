import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { LoadingController, AlertController } from '@ionic/angular';


@Component({
  selector: 'app-messagerie-client',
  templateUrl: './messagerie-client.page.html',
  styleUrls: ['./messagerie-client.page.scss'],
})
export class MessagerieClientPage implements OnInit {

  messageText: any;
  Client: any;
  user = JSON.parse(localStorage.getItem('user'));
  messages : Observable<any[]>;
  userClient: any;
  client: any;
  phone: any;
  mesmessages = [];
  mesMessagesSend = [];
  mesMessagesReceive = []
  mesMessages = [];
  MesMessages = [];
  image = "assets/Moi.jpg";
  imagePath: string;
  upload: any;

  constructor(
    private firestore: AngularFirestore,
    public loadingController: LoadingController,
    public alertController: AlertController,
    private camera: Camera
  ) {
    this.messages = this.firestore.collection('message').valueChanges();
    this.phone = " ";
    this.UserPhone(); 
   }

   async openLibrary() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetWidth: 1000,
      targetHeight: 1000,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    };
    return await this.camera.getPicture(options);
  }

  async openCamera() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetWidth: 1000,
      targetHeight: 1000,
      sourceType: this.camera.PictureSourceType.CAMERA
    };
    return await this.camera.getPicture(options);
  }

   async addPhoto(source: string){

    if (source === 'camera') {
      console.log('camera');
      const cameraPhoto = await this.openCamera();
      this.image = 'data:image/jpg;base64,' + cameraPhoto;
    } else {
      console.log('library');
      const libraryImage = await this.openLibrary();
      this.image = 'data:image/jpg;base64,' + libraryImage;
    }

    this.uploadFirebase();

   }

   async uploadFirebase(){

    // const loading = await this.loadingController.create({
    //   duration: 2000
    // });
    // await loading.present();
    // this.upload = this.firestore.collection('message_image').ref(this.imagePath).putString(this.image, 'message_image');
    // this.upload.then(async () => {
    //   await loading.onDidDismiss();
    //   this.image = 'assets/Moi.jpg';
    //   const alert = await this.alertController.create({
    //     header: 'Félicitation',
    //     message: 'L\'envoi de la photo dans Firebase est terminé!',
    //     buttons: ['OK']
    //   });
    //   await alert.present();
    // });

   }

  sendMessage(message){
    console.log(this.phone);
    this.firestore.collection('message').add({
      message_date : new Date().getTime(),
      message_receiver : "695797670",
      message_recipient : message,
      message_sender : this.phone,
      message_state: "send"
    });
    this.messageText = " ";
  }

  UserPhone(){

    this.firestore.collection('user').snapshotChanges(['added', 'removed', 'modified']).subscribe(actions => {
      this.userClient = actions.map(action => {
       if(action.payload.doc.data()['user_email'] ===  this.user.email){
        return{
          id : action.payload.doc.id,
          user_email: action.payload.doc.data()['user_email'],
          user_name: action.payload.doc.data()['user_name'],
          user_password: action.payload.doc.data()['user_password'],
          user_phone: action.payload.doc.data()['user_phone'],
          user_picture: action.payload.doc.data()['user_picture'],
          user_role:  action.payload.doc.data()['user_role'],
        }
       }
      });
      console.log(this.userClient);  
      this.userClient.forEach(r =>{
        if(r != undefined){
          this.client=r;
          console.log(this.client);
          this.phone = r.user_phone;
          console.log(this.phone);
        }   
      });
    }, err =>{
      console.log(err.message);
    });

  }

//  messagesReceive(){

//     this.mesmessages = [];
//     this.mesMessagesReceive = []
//     this.firestore.collection('message').snapshotChanges(['added', 'removed', 'modified']).subscribe(messages =>{
//       this.mesmessages = messages.map(mess => {
//         if(mess.payload.doc.data()['message_receiver'] === this.phone){
//           return {
//             id : mess.payload.doc.id,
//             message_date : mess.payload.doc.data()['message_date'],
//             message_receiver : mess.payload.doc.data()['message_receiver'],
//             message_sender : mess.payload.doc.data()['message_sender'],
//             message_state : "receive"
//           }
//         }
//       });
//       this.mesmessages.forEach(m =>{
//         if(m != undefined){
//           this.mesMessagesReceive.push(m);
//         }
//       });
//       console.log(this.mesMessagesReceive);
//       this.mesMessages = this.mesMessagesReceive;
//       console.log(this.mesMessages);
//     });

//   } 
  
  // messagesSend(){

  //   this.mesmessages = [];
  //   this.mesMessagesSend = [];
  //   this.firestore.collection('message').snapshotChanges(['added', 'removed', 'modified']).subscribe(messages =>{
  //     this.mesmessages = messages.map(mess => {
  //       if(mess.payload.doc.data()['message_sender'] === this.phone){
  //         return {
  //           id : mess.payload.doc.id,
  //           message_date : mess.payload.doc.data()['message_date'],
  //           message_receiver : mess.payload.doc.data()['message_receiver'],
  //           message_recipient : mess.payload.doc.data()['message_recipient'],
  //           message_sender : mess.payload.doc.data()['message_sender'],
  //           message_state : "receive"
  //         }
  //       }
  //     });
  //     console.log(this.mesmessages);
  //     this.mesmessages.forEach(m =>{
  //       if(m != undefined){
  //         this.mesMessagesSend.push(m);
  //       } 
  //     });
  //     console.log(this.mesMessagesSend);
  //     this.mesMessages = this.mesMessagesSend;
  //     console.log(this.mesMessages);
  //   });
    
  // }

  GetMessages(){
    this.mesmessages = [];
    this.mesMessages = [];
    this.firestore.collection('message').snapshotChanges(['added', 'removed', 'modified']).subscribe(messages =>{
      this.mesmessages = messages.map(mess => {
        if(mess.payload.doc.data()['message_sender'] === this.phone ||
               mess.payload.doc.data()['message_receiver'] === this.phone){
          return {
            id : mess.payload.doc.id,
            message_date : mess.payload.doc.data()['message_date'],
            message_receiver : mess.payload.doc.data()['message_receiver'],
            message_recipient : mess.payload.doc.data()['message_recipient'],
            message_sender : mess.payload.doc.data()['message_sender'],
            message_state : "receive"
          }
        }
      });
      console.log(this.mesmessages);
      this.mesmessages.forEach(m =>{
        if(m != undefined){
          this.mesMessages.push(m);
        } 
      });
      console.log(this.mesMessages);
      this.MesMessages = [];
      this.mesMessages.forEach((item, index) => {
        if(this.MesMessages.findIndex(i => i.id === item.id) === -1){
          this.MesMessages.push(item);
        }
      });
      console.log(this.MesMessages);
    });
  }

  ngOnInit() {
    // this.Client = JSON.parse(localStorage.getItem('Client'));

    // console.log(this.Client);

    // this.UserPhone();

    // this.messagesSend();

    // this.messagesReceive();  

    // this.GetMessages();
  }

  ionViewWillEnter(){
    this.GetMessages();
  }

}
