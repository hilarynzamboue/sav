import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { AuthentificationService } from './services/authentification.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  user = JSON.parse(localStorage.getItem('user'));
  userClient: any;
  client: any;
  constructor(
    public authService: AuthentificationService,
    private firestore: AngularFirestore
  ) {
    // this.firestore.collection('user').snapshotChanges(['added', 'removed', 'modified']).subscribe(actions => {
    //   this.userClient = actions.map(action => {
    //    if(action.payload.doc.data()['user_email'] ===  this.user.email){
    //     return{
    //       id : action.payload.doc.id,
    //       user_email: action.payload.doc.data()['user_email'],
    //       user_name: action.payload.doc.data()['user_name'],
    //       user_password: action.payload.doc.data()['user_password'],
    //       user_phone: action.payload.doc.data()['user_phone'],
    //       user_picture: action.payload.doc.data()['user_picture'],
    //       user_role:  action.payload.doc.data()['user_role'],
    //     }
    //    }
    //   });
    // }, err =>{
    //   console.log(err.message);
    // });
    // console.log(this.userClient);  
    // this.userClient.forEach(r =>{
    //   if(r != undefined){
    //     this.client=r;
    //     console.log(this.client);
    //   }   
    // });
  }

}
