import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthentificationService } from 'src/app/services/authentification.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  firename: string;
  firemail: string;
  firephone: string;
  firepwd: string;
  firepwdconfirm: string;
  firepicture: string;
  firerole = false;

  constructor(
    private firestore: AngularFirestore,
    public authService: AuthentificationService,
    public router: Router
  ) { }

  addFirebase(){
    this.firestore.collection('user').add({
      user_email: this.firemail,
      user_name: this.firename,
      user_password: this.firepwd,
      user_phone: this.firephone,
      user_picture: this.firepicture,
      user_role: this.firerole 
    });
  }

  ngOnInit() {
  }

  signUp(email, password){
    this.authService.RegisterUser(email, password)
    .then((res) => {
      console.log(res);
      this.authService.SendVerificationMail()
      this.router.navigate(['verify-email']);
    }).catch((error) => {
      window.alert(error.message)
    })

    this.addFirebase();
  }
  

}
