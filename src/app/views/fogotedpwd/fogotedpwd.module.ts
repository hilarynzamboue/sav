import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FogotedpwdPageRoutingModule } from './fogotedpwd-routing.module';

import { FogotedpwdPage } from './fogotedpwd.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FogotedpwdPageRoutingModule
  ],
  declarations: [FogotedpwdPage]
})
export class FogotedpwdPageModule {}
