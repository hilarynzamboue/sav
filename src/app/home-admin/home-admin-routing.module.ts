import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeAdminPage } from './home-admin.page';

const routes: Routes = [
  {
    path: '',
    component: HomeAdminPage,
    children: [
      {
        path: 'liste-clients',
        loadChildren: () => import('../views/Admin/liste-clients/liste-clients.module').then( m => m.ListeClientsPageModule)
      },
      {
        path: 'parametres',
        loadChildren: () => import('../views/parametres/parametres.module').then( m => m.ParametresPageModule)
      },
      {
        path: 'requetes-admin',
        loadChildren: () => import('../views/Admin/requetes-admin/requetes-admin.module').then( m => m.RequetesAdminPageModule)
      },
      {
        path: 'messagerie',
        loadChildren: () => import('../views/messagerie/messagerie.module').then( m => m.MessageriePageModule)
      },
      {
        path: '',
        redirectTo: 'liste-clients',
        pathMatch: 'full'
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeAdminPageRoutingModule {}
