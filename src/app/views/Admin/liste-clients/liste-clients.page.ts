import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { ActionSheetController } from '@ionic/angular';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-liste-clients',
  templateUrl: './liste-clients.page.html',
  styleUrls: ['./liste-clients.page.scss'],
})
export class ListeClientsPage implements OnInit {

  users : Observable<any[]>;

  constructor(
    private firestore: AngularFirestore,
    private actionSheetController: ActionSheetController,
    private router: Router
  ) { 
    this.users = this.firestore.collection('user').valueChanges();
  }

  sendMessage(c){
    localStorage.setItem('Client', JSON.stringify(c));
    this.router.navigate(['messagerie']);
  }

  async ShowSheet(pers) {
    const actionSheet = await this.actionSheetController.create({
      header: pers.user_name,
      buttons: [
       {
        text: 'Détails',
        role: '',
        icon: 'information-circle',
        handler: () => {
          console.log('Delete clicked');
          this.router.navigate(['infos'])
        }
      },{
        text: 'Supprimer',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          console.log('Delete clicked');
        }
      }, {
        text: 'Retour',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  ngOnInit() {
  }

}
