import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VueClientPage } from './vue-client.page';

const routes: Routes = [
  {
    path: '',
    component: VueClientPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VueClientPageRoutingModule {}
