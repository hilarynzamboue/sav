import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-messagerie',
  templateUrl: './messagerie.page.html',
  styleUrls: ['./messagerie.page.scss'],
})
export class MessageriePage implements OnInit {

  messageText: any;
  Client: any;
  user = JSON.parse(localStorage.getItem('user'));
  messages : Observable<any[]>;
  userClient: any;
  client: any =[];
  mesmessages = [];
  mesMessages = [];
  MesMessages = [];

  constructor(
    private firestore: AngularFirestore
  ) {
    this.messages = this.firestore.collection('message').valueChanges();
   }

  sendMessage(message){
    this.firestore.collection('message').add({
      message_date : "19/02/2022",
      message_receiver : this.Client.user_phone,
      message_recipient : message,
      message_sender : "695797670",
      message_state: "send"
    });
    this.messageText = " ";
  }

  GetMessages(){
    this.mesmessages = [];
    this.mesMessages = [];
    this.firestore.collection('message').snapshotChanges(['added', 'removed', 'modified']).subscribe(messages =>{
      this.mesmessages = messages.map(mess => {
        if((mess.payload.doc.data()['message_sender'] === this.Client.user_phone  &&
               mess.payload.doc.data()['message_receiver'] === "695797670") ||
               (mess.payload.doc.data()['message_sender'] === "695797670"  &&
                mess.payload.doc.data()['message_receiver'] === this.Client.user_phone)){
          return {
            id : mess.payload.doc.id,
            message_date : mess.payload.doc.data()['message_date'],
            message_receiver : mess.payload.doc.data()['message_receiver'],
            message_recipient : mess.payload.doc.data()['message_recipient'],
            message_sender : mess.payload.doc.data()['message_sender'],
            message_state : "receive"
          }
        }
      });
      console.log(this.mesmessages);
      this.mesmessages.forEach(m =>{
        if(m != undefined){
          this.mesMessages.push(m);
        } 
      });
      console.log(this.mesMessages);
      this.MesMessages = [];
      this.mesMessages.forEach((item, index) => {
        if(this.MesMessages.findIndex(i => i.id === item.id) === -1){
          this.MesMessages.push(item);
        }
      });
      console.log(this.MesMessages);
    });
  }

  ngOnInit() {
    this.Client = JSON.parse(localStorage.getItem('Client'));

    console.log(this.Client);
    // this.GetMessages();
  }

  ionViewWillEnter(){
    this.GetMessages();
  }

}
