import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActionSheetController, AlertController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { Request } from 'src/app/classes/request';
import { HashLocationStrategy } from '@angular/common';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.page.html',
  styleUrls: ['./accueil.page.scss'],
})
export class AccueilPage implements OnInit {

  requetes : Observable<any[]>;
  requete: Request;
  boutique: string;
  marequete: any;
  mesrequetes: any = [];
  Mesrequetes: any = [];
  MesRequetes: any = []; 
  user = JSON.parse(localStorage.getItem('user'));
  bout: string;

  constructor(
    public actionSheetController: ActionSheetController,
    private router:Router,
    public alertController: AlertController,
    private firestore: AngularFirestore,
  ) { 
    console.log(this.user.uid);
    this.requete = new Request();
    this.requetes = this.firestore.collection('request').valueChanges();
    this.ListRequetes();

  }

  ListRequetes(){

    this.mesrequetes = [];
    this.Mesrequetes = [];
    this.firestore.collection('request').snapshotChanges(['added', 'removed', 'modified']).subscribe(actions => {
      this.marequete = new Request();
      this.marequete = actions.map(action => {
       if(action.payload.doc.data()['request_sender'] ===  this.user.uid){
        return{
          id : action.payload.doc.id,
          NumeroSerie: action.payload.doc.data()['NumeroSerie'],
          boutique: action.payload.doc.data()['boutique'],
          description: action.payload.doc.data()['description'],
          etat: action.payload.doc.data()['etat'],
          produit: action.payload.doc.data()['produit'],
          request_date:  action.payload.doc.data()['request_date'],
          request_sender: action.payload.doc.data()['request_sender'],
          request_title: action.payload.doc.data()['request_title'],
        }
       }
      });
      console.log(this.marequete);
      this.mesrequetes = this.marequete;
      console.log(this.mesrequetes); 
      this.mesrequetes.forEach(r =>{
        if(r != undefined){
          this.Mesrequetes.push(r);
          console.log(this.Mesrequetes);
        }     
      });

      console.log(this.Mesrequetes);
      this.Mesrequetes.forEach((item, index) =>{
        if(this.MesRequetes.findIndex(i => i.id === item.id) === -1){
          this.MesRequetes.push(item)
        }
      });
      console.log(this.MesRequetes);

    }, err =>{
      console.log(err.message);
    });

  }

  async ShowSheet(pers) {
    const actionSheet = await this.actionSheetController.create({
      header: pers.nom,
      buttons: [
        {
          text: 'Message',
          icon: 'mail-sharp',
          handler: () => {
            console.log('Call clicked');
            this.router.navigate([
              'messagerie-client'])
          }
        }, {
        text: 'Détails',
        role: '',
        icon: 'information-circle',
        handler: () => {
          console.log('Delete clicked');
          this.router.navigate([
            'infos'])
        }
      }, {
        text: 'Partager',
        icon: 'share',
        handler: () => {
          console.log('Share clicked');
        }
      }, {
        text: 'Supprimer',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          console.log('Delete clicked');
        }
      }, {
        text: 'Retour',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  async NewRequest(){
    let alert = await this.alertController.create({
      header: 'Nouvelle requête',
      subHeader: 'Veillez remplir le formulaire',
      inputs: [
        {
          name: 'titre',
          placeholder: 'Titre de la requete',
          type: 'text',
        },
        {
          name: 'date',
          placeholder: 'Date (Format: jj/mm/aaaa)',
          type: 'text'
        },
        {
          name: 'produit',
          placeholder: 'Désignation du produit',
          type: 'text'
        },
        {
          name: 'numserie',
          placeholder: 'Numéro de série',
          type: 'text'
        },
        {
          name: 'boutique',
          placeholder: 'Boutique d\'achat',
          type: 'text'
        },
        {
          name: 'description',
          placeholder: 'Quel Problème rencontrez-vous?',
          type: 'textarea'
        }
      ],
      buttons: [
        {
          text: 'Annuler',
          handler: data => {
            console.log('Cancel clicked.');
          }
        },
        {
          text: 'Valider',
          role:'',
          handler: data => {
            console.log('Confirm clicked.');
            console.log(data);
            this.newRequest(data);
          }
        }
      ]
    });
    await alert.present();
  }

  // sendMessage(){
  //   this.router.navigate(['messagerie-client']);
  // }

  newRequest(req){
    this.firestore.collection('request').add({
      NumeroSerie : req.numserie,
      boutique : req.boutique,
      description : req.description,
      etat: 'warning',
      produit : req.produit,
      request_date : req.date,      
      request_sender : this.user.uid,
      request_title : req.titre,
    }).then(() => console.log("menu item is posted"))
    .catch((error) => alert("error: " + error));

  }

  ngOnInit() {

    // this.ListRequetes();

  }

  ionViewWillEnter(){
    this.ListRequetes();
    // this.sendMessage();
  }

}


