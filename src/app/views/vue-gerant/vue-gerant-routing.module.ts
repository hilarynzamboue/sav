import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VueGerantPage } from './vue-gerant.page';

const routes: Routes = [
  {
    path: '',
    component: VueGerantPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VueGerantPageRoutingModule {}
