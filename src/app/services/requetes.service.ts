import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class RequetesService {

  chemin='assets/requetes.json';

  constructor(private http: HttpClient) { }

  getRequetes(){
    return this.http.get(this.chemin);
  }

}
