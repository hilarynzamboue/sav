import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailsRequetePageRoutingModule } from './details-requete-routing.module';

import { DetailsRequetePage } from './details-requete.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailsRequetePageRoutingModule
  ],
  declarations: [DetailsRequetePage]
})
export class DetailsRequetePageModule {}
