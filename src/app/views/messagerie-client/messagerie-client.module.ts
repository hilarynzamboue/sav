import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MessagerieClientPageRoutingModule } from './messagerie-client-routing.module';

import { MessagerieClientPage } from './messagerie-client.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MessagerieClientPageRoutingModule
  ],
  declarations: [MessagerieClientPage]
})
export class MessagerieClientPageModule {}
