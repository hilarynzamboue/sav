import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AngularFireModule } from '@angular/fire/compat';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { Camera } from '@ionic-native/camera/ngx';



export const firebaseConfig = {
  apiKey: "AIzaSyCgwe6ktC4CC1y5tLUxP0vKErozPE2rcrY",
  authDomain: "projet-devmo-grp2.firebaseapp.com",
  databaseURL: "https://projet-devmo-grp2-default-rtdb.firebaseio.com",
  projectId: "projet-devmo-grp2",
  storageBucket: "projet-devmo-grp2.appspot.com",
  messagingSenderId: "201301215516",
  appId: "1:201301215516:web:f17d742e9debbc3f0dd2d9",
  measurementId: "G-86DBH840YZ"
};

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule,
     IonicModule.forRoot(), 
     AppRoutingModule,HttpClientModule,
     AngularFireModule.initializeApp(firebaseConfig),
     AngularFirestoreModule,
     AngularFireAuthModule,
    ],
  providers: [Camera, { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },HttpClient,
                  AngularFireAuth],
  bootstrap: [AppComponent],
})
export class AppModule {}
