import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { ActionSheetController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-requetes-admin',
  templateUrl: './requetes-admin.page.html',
  styleUrls: ['./requetes-admin.page.scss'],
})
export class RequetesAdminPage implements OnInit {

  requetes : Observable<any[]>;
  client : any;
  requete: Request;
  boutique: string;
  marequete: any;
  userClient: any;
  mesrequetes = [];
  Mesrequetes = [];
  user = JSON.parse(localStorage.getItem('user'));

  constructor(  public actionSheetController: ActionSheetController,
    private router:Router,
    private firestore: AngularFirestore,
    public alertController: AlertController ) { 
      this.requetes = this.firestore.collection('request').valueChanges();
    }


  async ChangeState(req){
    let alert = await this.alertController.create({
      header: req.request_title,
      subHeader: "Veillez choisir l'etat de la requete",
      inputs: [
        {
          label: 'Debut',
          type: 'radio',
          value: 'debut'
        },
        {
          label: 'En cours',
          type: 'radio',
          value: 'en cours'
        },
        {
          label: 'Terminer',
          type: 'radio',
          value: 'terminer'
        },
        {
          label: 'Echec',
          type: 'radio',
          value: 'echec'
        }
      ],
      buttons: [
        {
          text: 'Annuler',
          handler: data => {
            console.log('Confirm clicked.');
          }
        },
        {
          text: 'Valider',
          role:'',
          handler: data => {
            console.log('Confirm clicked.');
            if(data === "en cours"){
              req.etat = "secondary";
              // console.log(req);
              // this.firestore.doc('request'+'/'+req.id)
              // // req.etat = "secondary"; 
              console.log(req.NumeroSerie);
              this.firestore.collection('request').doc(req.NumeroSerie).update({
                etat: "secondary"
              }).catch(err =>{
                console.log(err.message);
              });
              // this.firestore.doc('request'+'/'+req.id).update();
              // this.firestore.collection('request').add({
              //   id : req.id,
              //   NumeroSerie : req.NumeroSerie,
              //   boutique : req.boutique,
              //   description : req.description,
              //   etat: "secondary",
              //   produit : req.produit,
              //   request_date : req.request_date,      
              //   request_sender : req.request_sender,
              //   request_title : req.request_title,
              // });
            }else if(data === "terminer"){
              req.etat = "success";
              // this.firestore.collection('request', ).add({
              //   NumeroSerie : req.numserie,
              //   boutique : req.boutique,
              //   description : req.description,
              //   etat: 'success',
              //   produit : req.produit,
              //   request_date : req.date,      
              //   request_sender : this.user.uid,
              //   request_title : req.titre,
              // });
            }else if(data === "echec"){
              req.etat = "primary";
              // this.firestore.collection('request', ).add({
              //   NumeroSerie : req.numserie,
              //   boutique : req.boutique,
              //   description : req.description,
              //   etat: 'primary',
              //   produit : req.produit,
              //   request_date : req.date,      
              //   request_sender : this.user.uid,
              //   request_title : req.titre,
              // });
            }else if(data === "debut"){
              req.etat = "warning";
              // this.firestore.collection('request', ).add({
              //   NumeroSerie : req.numserie,
              //   boutique : req.boutique,
              //   description : req.description,
              //   etat: 'warning',
              //   produit : req.produit,
              //   request_date : req.date,      
              //   request_sender : this.user.uid,
              //   request_title : req.titre,
              // });
            }
          }
        }
      ]
    });
    await alert.present();
  }  

  // .then(() => console.log("menu item is posted"))
  //   .catch((error) => alert("error: " + error));

  ngOnInit() {

  }

}
