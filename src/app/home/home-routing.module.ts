import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home.page';

const routes: Routes = 
[
  {
    path: '',
    component: HomePage,
    children: [
      {
        path: 'media',
        loadChildren: () => import('../views/media/media.module').then( m => m.MediaPageModule)
      },
      {
        path: 'parametres',
        loadChildren: () => import('../views/parametres/parametres.module').then( m => m.ParametresPageModule)
      },
      {
        path: 'accueil',
        loadChildren: () => import('../views/accueil/accueil.module').then( m => m.AccueilPageModule)
      },
      {
        path: '',
        loadChildren: () => import('../views/accueil/accueil.module').then( m => m.AccueilPageModule)

      },
    ]
  },
  {
    path: '',
    redirectTo: 'accueil',
    pathMatch: 'full'
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePageRoutingModule {}
